**A summer 2022 hit chiptune generator in pure Python (3)!!**
 
Which means you can simply run (Python 3 required):

`python python_chiptune.py`

and wait for it to generate the wave file (levitating_tune_2022.wav by default)

If you're on Linux, to make noise as soon as the programme is done you could do: (assuming SoX is installed):

`python python_chiptune.py && play levitating_tune_2022.wav` 

or (assuming aplay is installed and working):
  
`python python_chiptune.py && aplay levitating_tune_2022.wav`

 Dedicated to all Summer 2022 python and/or audio geeks :)
 

**HACKING:**
To change the tune look into the `tune.json` file, it should be quite self-explicative. The numbers represent standard MIDI note numbers. A -1 is a 'pause'

To change the sound look at the global parameters in the Python file.
For example the `NOTE_DURATION` determines the speed of the tune (note lower note duration, faster tempo!).
The `HARMONICS` parameter is a list of over/under-tones which can drastically change the timbre. The attack and decay parameters will also influence the sound.
