"""
Old school chiptune song wave generator in pure Python (3) - 2022 Edition

Enjoy!
Lorenzo.
"""

import wave
import struct
import math
import itertools
import operator
import json

# -------- GLOBAL CONSTANTS AND VALUES -------- #
SAMPLE_RATE = 44000         # sampe rate for the song
BIT_FORMAT = 2              # 1: 8 bit, 2: 16 bit
MAX_AMPLITUDE = (2 ** (BIT_FORMAT * 8) // 2) - 1
PACK_TYPE = {1: 'b', 2: 'h'}     # The type to use
MASTER_VOLUME = 1.0              # max volume
PI = math.pi
TWO_PI = 2 * PI
SQRT_12_2 = 1.0594630943592953  # 2 ** (1/12)
FREQ_FACTOR = 1                 # frequency factor: 1.0 is same midi notes etc.
CHANS = 2                       # Channels in wave file

# -------- SONG GLOBALS -------- #
# Single note duration in samples. This essentially defines the song 'tempo'
NOTE_DURATION = 5920
# Global trasnpose (semitones)
TRANSPOSE = 2
# Attack and decay factors for each note (as NOTE_DURATION ratio)
ATTACK_FACTOR = 0.01
DECAY_FACTOR = 0.18
PAUSE_NUMBER = -1       # Note number for a 'pause'

REPETITIONS = 1         # Repetitions of the song

# Not stricttly 'harmonics', more over- under-tones... They define timbre
# These are tuples of (frequency_ratio, intensity). So (1.0, 1.0) is the
# 'fundamental' and so on
HARMONICS = [
    (0.25, 0.3), (0.5, 0.85), (0.25, 0.42), (1.0, 1.0), (1.01, 0.93),
    (1.015, 0.9), (2.012, 0.8), (3, 0.4), (4.0, 0.2),(5.3, 0.1), (8, 0.2),
    (17, 0.04), (20, 0.02)
    ]

MELODY_FILE = "tune.json"
WAVE_OUT_FILE = "levitating_tune_2022.wav"

# Helper functions
def mtof(distance):
    """ See http://en.wikipedia.org/wiki/Equal_temperament for details...
Midi note number to frequency in Hertz.
distance is the distance (positive or negative) from A440 Hz key i.e. 69
    """
    p_a = 440.0
    a_ref = 69
    return p_a * (math.pow(SQRT_12_2, (distance - a_ref)))

def cycle_calc(freq, sample_rate, amplitude_factor=1.0):
    """ Calculate cycle for given frequency, optionally multiply by factor """
    return int(sample_rate / freq) / amplitude_factor

def make_ramp(min_i, max_i, size, amplitude):
    """ Make a linear ramp list of floats from begin to end of size """
    size = math.floor(size)
    interval_range = max_i - min_i
    single_step = (interval_range / size) * amplitude
    ramp = [min_i]
    for _ in range(1, size):
        ramp.append((ramp[_ - 1] + single_step))
    return ramp

def delay_delta(samples):
    """ Generate a list of zeros of length dureation (semples) """
    del_line = [0] * (int(samples))
    return del_line

def make_note(
        midi_num,
        note_duration,
        attack_list,
        decay_list,
        harmonic_list,
        amplitude=1,
        func=math.sin,
        pause_num=-1,
        frquency_factor=1.0,
        semple_rate=SAMPLE_RATE,
    ):
    """ Generate the list of samples for a single note (or a pause)
The func is the actual function for the wave. Default is sine.

harmonic_list is a list of tuples each representing overtone ratio and amplitude
for example: [(1.0, 1.0), (2.0, 0.5)] will contain the first overtone with
amplitude 1.0 and the second harmonic (overtone with ratio 2.0) with half ampl.
    """
    # This was useful: https://v.gd/sBGS14
    # Is this a pause. In such case we just return a list o zeros?
    if midi_num == pause_num:
        return [0] * note_duration

    sample_list = []
    note_freq = mtof(midi_num)

    # cycle_list contains the cycles for the note based on the harmonics
    cycle_list = []

    for harm, amp in harmonic_list:
        this_cycle = cycle_calc(note_freq * harm, semple_rate, frquency_factor)
        cycle_list.append((this_cycle, amp))

    decay_duration = len(decay_list)

    # Here we generate the actual samples
    for i in range(0, note_duration):
        # Are we within the attack samples? If so scale the volume accordingly
        if i < len(attack_list):
            volume = attack_list[i]

        # Similarly if we are within decay samples (at the end)...
        if note_duration - i <= decay_duration:
            volume = decay_list[note_duration - i - 1]

        # Finally calculate sample value and append to value list
        volume_const = amplitude * volume * (2 / len(cycle_list))

        value = 0
        for (cycle, amp) in cycle_list:
            this_sample = int(
                func((i % cycle) / cycle) * volume_const * amp
            )
            value += this_sample

        sample_list.append(value)
    return sample_list

def make_melody(note_list, sample_dictionary):
    """ Generate a list of samples from notes and the notes dictionary list """
    sample_list = []

    for note in note_list:
        sample_list.extend(sample_dictionary[note])
    return sample_list

def transpose(note_list, semitones):
    """ return note list transposed of semitones, ignoring pauses (i.e. -1) """
    return [x + semitones if x >= 0 else -1 for x in note_list]

def load_tune(json_file):
    """ Load the tune from json file and return a list of parts """
    # Parts are currenrly hardcoded to 2 voices!!!
    with open(json_file, 'r', encoding='UTF-8') as j_f:
        part1 = []
        part2 = []
        data = json.load(j_f)
        parts = data["parts"]
        structure = data["structure"]
        for part in structure:
            part1.extend(parts[part][0])
            part2.extend(parts[part][1])
        if TRANSPOSE != 0:
            part1 = transpose(part1, TRANSPOSE)
            part2 = transpose(part2, TRANSPOSE)
        return[part1, part2]

PARTS = load_tune(MELODY_FILE)


if __name__ == "__main__":
    print("Calculating notes...")
    # precompute lists of attack and decay amplitudes for each sample
    attack_amps = make_ramp(
        0, MAX_AMPLITUDE, NOTE_DURATION * ATTACK_FACTOR, MASTER_VOLUME
        )
    decay_amps = make_ramp(
        0, MAX_AMPLITUDE, NOTE_DURATION * DECAY_FACTOR, MASTER_VOLUME)



    # pre-compute only samples for needed notes and store them in a dictionary
    # First we extract only the unique notes by converting the list to a set..
    # This speeds up the generation process quite a lot!!
    unique_notes = set(itertools.chain.from_iterable(PARTS))
    note_dictionay = {}
    for this_note in unique_notes:
        note_dictionay[this_note] = make_note(
            this_note,
            NOTE_DURATION,
            attack_amps,
            decay_amps,
            HARMONICS,
            func=math.atan
        )

    print("Making the melodies... satndby")
    melody_list = []

    for this_part in PARTS:
        this_melody = make_melody(this_part, note_dictionay)
        melody_list.append(this_melody)

    print("DJ at the mixer...")
    sample_sum = list(map(operator.add, melody_list[0], melody_list[1]))

    print("Delay... elay... lay... ay...")
    final_samples = []

    # Generate delay lines to simulate an "echo" effect
    DELAY1 = delay_delta(NOTE_DURATION * 1.25)
    DELAY2 = delay_delta(NOTE_DURATION * 1.75)
    DELAY3 = delay_delta(NOTE_DURATION * 2.25)
    DELAY4 = delay_delta(NOTE_DURATION * 2.5)
    DELAY1.extend([x * 0.046 for x in sample_sum])
    DELAY2.extend([x * 0.03 for x in sample_sum])
    DELAY3.extend([x * 0.01 for x in sample_sum])
    DELAY4.extend([x * 0.009 for x in sample_sum])

    print("The Magic of Stereo  -)]-[(- ...")
    # Interleave for stereo...
    for index, sample in enumerate(sample_sum):
        # Echo-type effect with one delay line on each stereo channel
        c1 = sample  + DELAY1[index] + DELAY3[index]
        c2 = sample  + DELAY2[index] + DELAY4[index]
        final_samples.append(c1)
        final_samples.append(c2)

    print("Packing the data [][][][][]...")
    packed_values = map(
        lambda x: struct.pack(PACK_TYPE[BIT_FORMAT], int(x * 0.3)), final_samples
    )
    VALUE_STR = (b''.join(packed_values) * REPETITIONS)

    wave_output = wave.open(WAVE_OUT_FILE, 'w')
    wave_output.setparams((CHANS, BIT_FORMAT, SAMPLE_RATE, 0, 'NONE', None))
    wave_output.writeframes(VALUE_STR)
    wave_output.close()

    print(f"\nYour masterpiece is in {WAVE_OUT_FILE}")
    ART = """
          ===================
        ===================--;
        ;                    ;
        ;                    ;
        ;                    ;
        ;                    ;
        ;                .;;;;
    .;;;;                ';;;;'             
    ';;;;'               `;;;;'
    `;;;;'

    -----==== Enjoy ! ====-----

    """
    print(ART)
